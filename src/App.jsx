import React from 'react';
import { Provider, useSelector, useDispatch } from 'react-redux';
import { store } from './store';
import Modal from 'react-modal';

const RelatedProducts = React.lazy(() => import('Products/App'));

const App = () => {
    return (
        <Provider store={store}>

            <React.Suspense fallback={null}>
                <Cart />
                <Products />
            </React.Suspense>
        </Provider>
    );
}
const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        width: '70vw'
    },
};

Modal.setAppElement('#modal');
const Cart = () => {
    const [modalIsOpen, setIsOpen] = React.useState(false);
    const hiddenCart = () => {
        setIsOpen(false);
    }
    const openCart = () => {
        setIsOpen(true);
    }
    const carts = useSelector((state) => state.productsReducer.carts);
    return (
        <header className='w-full  h-12'>
            <div className='full max-w-6xl flex mx-auto my-3'>
                <button onClick={openCart} className='px-5 py-3 bg-red-400 rounded-2xl text-white flex relative'>Giỏ hàng
                    <pre className='top-0 text-[13px] ml-3'>{carts.length} </pre>
                </button>
            </div>
            <Modal
                isOpen={modalIsOpen}

                style={customStyles}
                contentLabel="Example Modal"
            >
                <h2 >Giỏ hàng</h2>
                <button onClick={hiddenCart}>Đóng</button>
                <div>
                    {
                        carts.map((item, key) => (
                            <div className='flex' key={key}>
                                <div className='flex-1'>
                                    {item.title}
                                </div>
                                <div className='px-4 '>
                                    {item.price}
                                </div>
                                <div className='px-4'>
                                    {item.number}
                                </div>
                                <div className='px-4'>
                                    {item.number * item.price}
                                </div>
                            </div>
                        ))
                    }

                </div>
            </Modal>
        </header>
    )
}
const ADD_TO_CART = 'ADD TO CART'
const Products = () => {
    const dispatch = useDispatch();
    const productId = useSelector((state) => state.productsReducer.productId);
    const products = useSelector((state) => state.productsReducer.products);
    const relatedProductId = useSelector((state) => {
        return state && state.relatedReducer && state.relatedReducer.productId ? state.relatedReducer.productId : null
    });
    const product = products.find(product => product.id === (relatedProductId || productId));
    const addToCart = (product) => {
        dispatch({ type: ADD_TO_CART, payload: product });
    }
    return (

        <div style={{ width: 1000 }} className="relative container mx-auto flex flex-row m-10 border-dashed border-2 border-red-500 rounded">
            <div className="absolute -top-7 text-red-500 font-bold">Team Core (<a target="_blank" href="https://github.com/dotuan9x/micro-frontends/tree/master/react-redux/container" rel="noreferrer">container</a>) </div>
            <div className="flex flex-col w-full p-5">
                <div className="flex flex-row w-full justify-between">
                    <h1 className="text-lg font-bold">Shop Quần áo</h1>
                    <div>Orders: 0 items</div>
                </div>
                <div className="flex flex-row">
                    <div className="w-2/3">
                        <img alt="" src={`${product.image}`} />
                    </div>
                    <div className="w-1/3 pt-10">
                        <label className="text-lg font-medium">{product.title}</label>
                        <ul className="flex flex-row mt-10 space-x-3">
                            {
                                product.images.map((item, index) => (
                                    <li key={`${product.id}${index}`} className="cursor-pointer border-b-2 border-white hover:border-gray-300 flex-1 h-15">
                                        <img className='object-cover' alt="" src={`/assets/${item}`} />
                                    </li>
                                ))
                            }
                        </ul>
                        <button className="font-medium hover:bg-gray-50 border border-gray-300 rounded p-3 mt-10" onClick={() => addToCart(product)}>Thêm vào giỏ hàng</button>
                    </div>
                </div>
            </div>
            <div className="p-1">
                <RelatedProducts store={store} />
            </div>
        </div>

    )
}

export default App;