const initialState = {};

const ADD_TO_CART = 'ADD TO CART'

export const updateProductId = (productId) => {
    return { type: ADD_TO_CART, payload: productId };
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_TO_CART: {
            return {
                ...state,
                productId: action.payload,
            };
        }
    }

    return state;
};

export default reducer;